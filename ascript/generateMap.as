﻿	var gameInfo:Array = new Array();
	gameInfo["start"] = false;
	map_background.gameInfo = gameInfo;
	
	
	//randomize number from MIN to MAX
	
	function randomnumber(max:int=10, min:int=0, range:int=0){
		var numer:int = Math.random()*max;
		
		if(numer>=max-range){
			numer = max-range;
		}
		
		if(numer<=min+range){
			numer = min+range;
		}
		//trace(numer);
		return numer;
	}

	
	//DRAW hexogens around one element and returns drawed element count

	function drawaround(yCord:int,xCord:int,Map:Array,yMax:int=14,xMax:int=29,continent:int=0){
		
		//yCord:int,xCord:int,Map:Array,drawcount:int,player:int
		//yCord:int,xCord:int,Map:Array
		
		
		var axeX:int;
		var axeY:int;
		var xnum:int;
		var ynum:int;
		var found:int=0;
		var loopcount:int;
		
		
		var axe:Array = [-1,0,1];
		
		var p:int=0;
		while(p<1){
			
			axeX = axe[randomnumber(3)];
			axeY = axe[randomnumber(3)];
			
			//this if means that one of the coordinates have to be 0. (+) formation.
			if(axeX==0 || axeY==0){
			
				if(axeX==0 && axeY==0){
					
				}
				else{
					axeY = yCord +axeY;
					axeX = xCord +axeX;
					
					if(axeY>=0 && axeX>=0 && axeY<yMax && axeX<xMax){
					
						if(Map[axeY][axeX]==continent){
							
							found = 1;
							p++;
						}
					}
				}
			}
			loopcount++;
			
			if(loopcount>9){
				axeY = yCord;
				axeX = xCord;
				p++;
			}
			
		}
		
		return [axeY, axeX, found];
	}
	
	
	
	//Check for used hexogens and returns array with theyr cordinates 
	
	function usedHex(Map:Array, mapwidth:int, maphight:int, continent:int){
			
			var usedX:Array = new Array();
			var usedY:Array = new Array();
			
			for(var i=0; i<maphight; i++){
				for(var j=0; j<mapwidth; j++){
					if(Map[i][j]==continent){
						
						usedX.push(j);
						usedY.push(i);
						
					}
				}
			}
			return [usedX, usedY];
	}
	
	//remove duplicate from array
	
	function removeDuplicate(arr:Array) : void{
		
		var i:int;
		var j: int;
		
		for (i = 0; i < arr.length - 1; i++){
			for (j = i + 1; j < arr.length; j++){
				if (arr[i] === arr[j]){
					arr.splice(j, 1);
				}
			}
		}
	}
	
	//Filter from map array all continents neighbors.
	
	function filterneighbors(Map:Array, continent:int = 0):Array{
		
		var i:int = 0;
		var j:int = 0;
		var n:int = 0;
		var val = Map[i][j];
		var neibghors:Array = new Array();
		var temp:Array = new Array();
		
		/*
		trace("plotis"+Map.length);
		trace("aukstis"+Map[0].length);
		trace("map x plotis"+Map[23][47]);
		*/
		
		
		
		for(i=0; i<Map.length; i++){
			
			for(j=0; j<Map[i].length; j++){
				n=0;
				
				while(n<6){
					
					switch (n) { 
    				case 0:
						if(j+1<Map[i].length){
							val = Map[i][j+1];
						}
                 		break ;
					case 1:
						val = Map[i][j-1];
						break;
					case 2:
						if(i+1<Map.length){
							val = Map[i+1][j];
						}
						break;
					case 3:
						if(i-1>0){
							val = Map[i-1][j];
						}
						break;
					case 4:
						//Reik kad tikrintu x kyginis ar nelyginis
						if(i+1<Map.length && j+1<Map[i].length && j % 2 == 0){
							val = Map[i+1][j+1];
						}
						if(i-1>0 && j-1>0 && j % 2 != 0){
							val = Map[i-1][j-1];
						}
						break;
					case 5:
						//Reik kad tikrintu x kyginis ar nelyginis
						if(i-1>0 && j+1<Map[i].length && j % 2 == 0){
							val = Map[i-1][j+1];
						}
						if(i+1<Map.length && j-1>0 && j % 2 != 0){
							val = Map[i+1][j-1];
						}
						break;
    				} 
				
					
					
					if(Map[i][j]!=val && Map[i][j]!=0 && val!=0 && j+1<Map[i].length){
							//trace("kaimynai kai x="+i+" "+Map[i][j]+" su "+Map[i][j+1]);
							
							if(neibghors[""+Map[i][j]+""]){
								
								
								
								temp = neibghors[""+Map[i][j]+""];
								
								//trace("WTF???"+temp[1]);
								
								removeDuplicate(temp);
								
								temp.push(val);
								
								removeDuplicate(temp);
								
								neibghors[""+Map[i][j]+""] = temp;
								temp = new Array();
								
							}
							else{
								neibghors[""+Map[i][j]+""] = [val];
							}
							
							
					}
				n++;
				}
			
			//trace("coordinates "+i+" "+j+":"+Map[i][j]);
			}
			
			
		}
		/*
		for(i=1; i<neibghors.length; i++){
			trace(i+" kaimynai: "+neibghors[i][0]);
		}
		*/
		
		
		/*if(continent != 0){
			trace(continent+" kaimynai: "+neibghors[""+continent+""][0]);
			temp = neibghors[continent];
			return [temp];
			
		}
		else{
			
			
		}*/
		return neibghors;
		
	}
	
	
	
	
	//DRAW continents

	function drawcontinents(continentcount:int, side:int, Map:Array, mapstartx:int, mapstarty:int, elementdrawx:int, elementdrawy:int){
		/*
		trace("count "+continentcount);
		*/
		var n:int=1;
		var i:int = 0;
		var j:int = 0;
		var k:int = 0;
		var color:int;
		var player:int;
		var maxplayers:int = 2;
		var bordercolor:int = 0x000000;
		var opacity:int = 0;
		var continent:MovieClip;
		
		if(map_background.getChildByName("continent"+n)!=null){
			
			while(n<=continentcount){
				map_background.removeChild(map_background.getChildByName("continent"+n));
				n++;
			}
		
		}
		
		n=1;
		
		var neibghours:Array = filterneighbors(Map);
		
		var generatedmapbackground:MovieClip = new MovieClip();
		
		//!!!!!!!!!!!!!!!! PAVEIXLIOKA kap texturas nauduojems  !!!!!!!!!1
		
		var bitmap = new maptexture3(0,100);
		generatedmapbackground.graphics.beginBitmapFill( bitmap, null, true, false );
		
		//-------------------------------------------------
		
		map_background.addChild(generatedmapbackground);
		
		var dropShadow:DropShadowFilter = new DropShadowFilter();
		dropShadow.color = 0x000000;
		dropShadow.blurX = 3;
		dropShadow.blurY = 3;
		dropShadow.angle = 120;
		dropShadow.alpha = 0.6;
		dropShadow.distance = 3;
		dropShadow.inner = false;
		
		
		var bgglow:GlowFilter = new GlowFilter(); 
		bgglow.color = 0x000000;
		bgglow.strength = 2;
		bgglow.alpha = 1;
		//glow.knockout = true;
		bgglow.blurX = 2;
		bgglow.blurY = 2;
		bgglow.inner = true;
 
		generatedmapbackground.filters = [bgglow , dropShadow];
			
		
		
		while(n<=continentcount){
			
			continent = new MovieClip();
			continent.useHandCursor = true;
			continent.alpha = 0.6;
			continent.name = "continent"+n;
			var minX:Number = 20000;
			var minY:Number = 20000;
			var maxX:Number = 0;
			var maxY:Number = 0;
			
			//trace("continentas "+continent.name);
			
			
			for(i=0; i<elementdrawy; i++){
				for(j=0; j<elementdrawx; j++){
					
					
					
					if(Map[i][j]==n){
						
						
						if(mapstartx+((side*1.5)*j)<minX){
							minX = mapstartx+((side*1.5)*j);
						}
						
						if(mapstartx+((side*1.5)*j)>maxX){
							maxX = mapstartx+((side*1.5)*j);
						}
						
						if(mapstarty+k+(side*2*i)<minY){
							minY = mapstarty+k+(side*2*i);
						}
						
						if(mapstarty+k+(side*2*i)>maxY){
							maxY = mapstarty+k+(side*2*i);
						}
						
						if(j % 2 != 0){
							k=side;
							//k=0;
						} 
						
						if(Map[i][j]>0){
							if(Map[i][j]%2 != 0){
								player = 1;
								color = 0x8d1d1e;
								bordercolor = 0xAAAAAA;
								opacity = 1;
							}
							else{
								player = 2;
								color = 0x454332;
								bordercolor = 0xAAAAAA;
								opacity = 1;
							}
						}
						
						//continent.graphics.lineStyle(1,0x000000);
						continent.graphics.beginFill(color, opacity);
						
						
						continent.graphics.moveTo(mapstartx+((side*1.5)*j), (mapstarty+k+(side*2*i)));
						continent.graphics.lineTo(mapstartx+side+((side*1.5)*j), (mapstarty+k+(side*2*i)));
						continent.graphics.lineTo(mapstartx+side+(side/2)+((side*1.5)*j), (mapstarty+k+side+(side*2*i)));
						continent.graphics.lineTo(mapstartx+side+((side*1.5)*j), (mapstarty+k+(side*2)+(side*2*i)));
						continent.graphics.lineTo(mapstartx+((side*1.5)*j), (mapstarty+(side*2)+k+(side*2*i)));
						continent.graphics.lineTo(mapstartx-(side/2)+((side*1.5)*j), (mapstarty+k+side+(side*2*i)));
						continent.graphics.lineTo(mapstartx+((side*1.5)*j), (mapstarty+k+(side*2*i)));
						continent.graphics.endFill();
						
						
						
						
						generatedmapbackground.graphics.moveTo(mapstartx+((side*1.5)*j), (mapstarty+k+(side*2*i)));
						generatedmapbackground.graphics.lineTo(mapstartx+side+((side*1.5)*j), (mapstarty+k+(side*2*i)));
						generatedmapbackground.graphics.lineTo(mapstartx+side+(side/2)+((side*1.5)*j), (mapstarty+k+side+(side*2*i)));
						generatedmapbackground.graphics.lineTo(mapstartx+side+((side*1.5)*j), (mapstarty+k+(side*2)+(side*2*i)));
						generatedmapbackground.graphics.lineTo(mapstartx+((side*1.5)*j), (mapstarty+(side*2)+k+(side*2*i)));
						generatedmapbackground.graphics.lineTo(mapstartx-(side/2)+((side*1.5)*j), (mapstarty+k+side+(side*2*i)));
						generatedmapbackground.graphics.lineTo(mapstartx+((side*1.5)*j), (mapstarty+k+(side*2*i)));
						
						
						//generatedmapbackground.graphics.copyFrom(continent.graphics);
						
						
						k=0;
					
					}
					
					
						// random color: color = 0x000000 + Math.random() * 0xFFFFFF;
					
				}
			}
			
			var myText:TextField = new TextField();
			myText.name = "continent"+n+"text";
			myText.text = ""+n+"";
			myText.width = 20;
			myText.height = 20;
			myText.x = maxX-((maxX-minX)/2);
			myText.y = maxY-((maxY-minY)/2);
			myText.selectable = false;
			myText.mouseEnabled = false;
			continent.addChild(myText);
			
			//trace(myText.name);
			
			
			
			
			map_background.addChild(continent);
			
			//continent.TextField.text("vo bbz"+n);
			
			var contInfo:Array = new Array();
			
			contInfo["player"]=player;
			contInfo["units"]=n;
			contInfo["color"]=color;
			contInfo["bordercolor"]=bordercolor;
			
			contInfo["clicked"]=false;
			
			contInfo["neibghours"] = neibghours[n];
			
			
			//trace("kaimynai"+neibghours[0]);
			
			//contInfo["neibghors"]=neibghours;
			
			continent.contInfo = contInfo;
			
			//trace("x="+continent.x+" y="+continent.y);
			
			
			

			var glow:GlowFilter = new GlowFilter(); 
			glow.color = bordercolor;
			glow.strength = 10;
			//glow.knockout = true;
			glow.alpha = 10;
			glow.blurX = 1.5;
			glow.blurY = 1.5;
			glow.inner = true;
 
			continent.filters = [glow];
			/*
			var dropShadow:DropShadowFilter = new DropShadowFilter();
			dropShadow.color = 0x000000;
			dropShadow.blurX = 2;
			dropShadow.blurY = 2;
			dropShadow.angle = 0;
			dropShadow.alpha = 1;
			dropShadow.distance = 0;
			dropShadow.inner = false;
			
			continent.filters = [dropShadow];
			*/
		n++;
		}
		
		generatedmapbackground.graphics.endFill();
		//PLAYER TURN
		
		map_background.gameInfo["playerturn"] = 1;
		
		if(!stage.getChildByName("playerturnText")){
		
			myText.name = "playerturnText";
			myText.text = map_background.gameInfo["playerturn"]+" Player Turn";
			
			myText.x = 160;
			myText.y = 13;
			myText.width = 150;
			myText.height = 20;
			myText.selectable = false;
			myText.mouseEnabled = false;
			stage.addChild(myText);
			myText.textColor = 0xFFFFFF;
		
		}
		
		map_background.gameInfo["maxplayers"] = maxplayers;
		map_background.gameInfo["start"] = true;
		
	}
	
	
	//Creates map array
	
	function generateMap (e:MouseEvent):void{
	
	
	if (map_background.getChildByName("hexagon")!= null){
		
		map_background.removeChild(map_background.getChildByName("hexagon"));
	}
	
	var side = 10;
	
	var maxhexcount:int = 30;
	var continentcount:int = 31;
	
	var framesize = 10+side;
	var k = 0;
	
	var mapwidth = map_background.width - framesize-side;
	var mapheight = map_background.height - framesize-side;
	var mapstartx = framesize;
	var mapstarty = framesize;
	
	var elementdrawx:int = mapwidth / (side*1.5);
	var elementdrawy:int = mapheight / (side*2);
	var elementcount = elementdrawx * elementdrawy;
	
	
	/*
	trace("x element count = "+elementdrawx+
		  " | y element count = "+elementdrawy+
		  " | all element count = "+elementcount);
	*/
	
	
	
	
	
	var Row:Array = new Array();
	var Map:Array = new Array();
	
	var randomnum:int;
	
	for(var i=0; i<elementdrawy; i++){
		for(var j=0; j<elementdrawx; j++){
			
			//randomnum = Math.random()*(3+1);
			randomnum = 0;+-
			Row.push(randomnum);
			
		}
		
		Map.push(Row);
		Row=[];
	}
	
	
	
	
	//DRAW normal continent
	
	
	
	var yCord:int = 0;
	var xCord:int = 0;
	
	
	for(j=1; j<=30; j++){
		
		Map[yCord][xCord]=j;
		
		var hexcount:int=0;
		var oldhexcount:int=0;
		var needhex:int=maxhexcount;
		var Cord:Array;
		var aroundpnts:Array;
		var safer:int=0;
		var resets:int=0;
		
		while(hexcount<maxhexcount){
			aroundpnts = usedHex(Map, elementdrawx, elementdrawy,j);
			for(i=0; i<aroundpnts[1].length; i++){
				if(hexcount<maxhexcount){
					Cord = drawaround(aroundpnts[1][i],aroundpnts[0][i],Map,elementdrawy, elementdrawx);
				
					if(Cord[2]==1){
						Map[Cord[0]][Cord[1]]=j;
						hexcount++;
					}
				}
				
			}
			safer++;
			if(safer*3>maxhexcount && hexcount<maxhexcount*0.85){
				
				aroundpnts = usedHex(Map, elementdrawx, elementdrawy,j);
				
				for(i=0; i<aroundpnts[1].length; i++){
					Map[aroundpnts[1][i]][aroundpnts[0][i]]=0;
				}
				
				aroundpnts = usedHex(Map, elementdrawx, elementdrawy,0);
				
				Map[aroundpnts[1][randomnumber(aroundpnts[1].length)]][aroundpnts[0][randomnumber(aroundpnts[1].length)]]=j;
				
				resets = resets +1;
				safer = 0;
				/*
				trace("old hexcount["+j+"]="+hexcount+" ilgis:"+aroundpnts.length);
				*/
				hexcount = 1;
				
				//Cord = drawaround(Cord[0],Cord[1],Map);
				//Map[yCord][xCord]=j;
				
				
				
				
			}
			if(resets>maxhexcount){
				aroundpnts = usedHex(Map, elementdrawx, elementdrawy,j);
				
				for(i=0; i<aroundpnts[1].length; i++){
					Map[aroundpnts[1][i]][aroundpnts[0][i]]=0;
				}
				oldhexcount = hexcount;
				hexcount = maxhexcount;
			}
			oldhexcount = hexcount;
									   
		}
		
		/*
		trace("ratu "+safer+" "+j);
		*/
		
		
		Cord = drawaround(Cord[0],Cord[1],Map);
		/*
		trace(Cord);
		*/
		yCord = Cord[0];
		xCord = Cord[1];
			
	
		/*if(Cord[2]==0){
			aroundpnts = usedHex(Map, elementdrawx, elementdrawy,j);
				
				for(i=0; i<aroundpnts[1].length; i++){
					Map[aroundpnts[1][i]][aroundpnts[0][i]]=0;
				}
				
				aroundpnts = usedHex(Map, elementdrawx, elementdrawy,0);
				
				yCord = aroundpnts[1][randomnumber(aroundpnts[1].length)];
				xCord = aroundpnts[0][randomnumber(aroundpnts[1].length)];
				
		}*/
		
		/*
		trace("hexcount["+j+"]="+oldhexcount);
		*/
		
		
		
		
	}
	
	drawcontinents(continentcount,side, Map, mapstartx, mapstarty, elementdrawx, elementdrawy);
	
}