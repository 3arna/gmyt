﻿	
	
	
	
	function endTurn(e:MouseEvent){
		
		var playerturn = map_background.gameInfo["playerturn"];
		playerturn++;
		
		if(playerturn > map_background.gameInfo["maxplayers"]){
			playerturn = 1;
		}
		
		var playerturnText = stage.getChildByName("playerturnText");
		playerturnText.text = playerturn+" Player Turn";
		resetallCont();
		map_background.gameInfo["playerturn"]=playerturn;
		
	}