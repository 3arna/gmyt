﻿	
	//Change continent glow
	
	function glowColor(continent:Object, color:Number){
		
		var glow:Array = continent.filters;
		glow[0].color = color;
		continent.filters = glow;
	}
	
	//Reset continent to normal state.
	
	function resetCont(continent:Object){
		glowColor(continent, continent.contInfo["bordercolor"]);
		continent.getChildByName(continent.name+"text").text = continent.contInfo["units"];
		continent.contInfo["clicked"]=false;
	}
	
	//set Neigbhours to bordered state
	
	function setNeigbhours(continent:Object){
		
		var n:int = 0;
		var temp:Object;
		while(n<continent.contInfo["neibghours"].length){
						
			if(continent.contInfo["neibghours"][n]){
						
				temp = map_background.getChildByName("continent"+continent.contInfo["neibghours"][n]);
				temp.contInfo["checked"] = true;
				
				glowColor(temp, 0x000000);
			}
						
						
						
			n++;
		}
	}
	
	//Reset all continent states to normal.
	
	function resetallCont(){
		var n:int = 1;
		var continent:Object;
		
		while(map_background.getChildByName("continent"+n)!=null){
			
			continent = map_background.getChildByName("continent"+n);
					
			if(continent.contInfo["clicked"] || continent.contInfo["checked"]){
						
				glowColor(continent, continent.contInfo["bordercolor"]);
				continent.getChildByName(continent.name+"text").text = continent.contInfo["units"];
				continent.contInfo["clicked"]=false;
				continent.contInfo["checked"]=false;
						
			}
				
			n++;
		}
	}
	
	
	
	//what is happening when u click on continent.
	
	function continentClick(e:MouseEvent){
		
		var continent:Object;
		
		
		
		if(map_background.gameInfo["start"]){
			
			
			continent = e.target;
			
			if(map_background.gameInfo["playerturn"]==continent.contInfo["player"]){
			
				
				
				if(continent.contInfo["clicked"]==true){
					resetallCont();
					//resetCont(continent);
				}
				else{
					
					resetallCont();
					
					continent = e.target;
					
					//trace(continent.contInfo["neibghors"][1]);
					
					continent.getChildByName(continent.name+"text").text = "T";
					glowColor(continent, 0xFFFFFF);
					continent.contInfo["clicked"]=true;
					
					setNeigbhours(continent);
					
				}
			}
			
		}
		else{
			trace("map not generated");
		}
	}